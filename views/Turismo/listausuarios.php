<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>/bootstrap/img/icono.png">
  <link rel="icon" type="image/png" href="<?=base_url()?>/bootstrap/img/icono.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Lista de Usuarios
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="<?=base_url()?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?=base_url()?>/bootstrap/css/paper-kit.css?v=2.2.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?=base_url()?>/bootstrap/demo/demo.css" rel="stylesheet" />
</head>

<body class="register-page sidebar-collapse">

  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg fixed-top navbar-transparent " color-on-scroll="300">
    <div class="container">
      
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <ul class="navbar-nav">
          
          <li class="nav-item">
          <a href="<?=base_url()?>index.php/Welcome/index" class="btn btn-danger btn-round"><i class="nc-icon nc-tap-01"></i> Inicio</a>
          </li>
          <li class="nav-item">
            <a href=""  class="btn btn-danger btn-round"><i class="nc-icon nc-image"></i> Fotografias</a>
          </li>
          <li class="nav-item">
            <a href=""  class="btn btn-danger btn-round"><i class="nc-icon nc-send"></i> Lugares Turisticos</a>
          </li>
          <li class="nav-item">
            <a href=""  class="btn btn-danger btn-round"><i class="nc-icon nc-bus-front-12"></i> Paradas</a>
          </li>
          <li class="nav-item">
            <a href=""  class="btn btn-danger btn-round"><i class="nc-icon nc-paper"></i> Historia</a>
          </li>
          <li class="nav-item">
            <a href="Turismo/index.php"  class="btn btn-danger btn-round"><i class="nc-icon nc-single-02"></i> Lista de Usuarios</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  
 
  <div class="container">
    <h2>.</h2>
    <?php echo form_open_multipart('welcome/registro');?>
    <h3>Nuevo Usuario</h3>
    <button type="submit" class="btn btn-primary">Agregar</button>
    <?php echo form_close(); ?>
    <table class="table">
      <thead>
        <tr>
          <th>No.</th>
          <th>A.Paterno</th>
          <th>Nombres</th>
          <th>Email</th>
          <th>C.I.</th>
          <th>Login</th>
          <th>Password</th>
          <th>Estado</th>
          <th>Modificar</th>
          <th>Eliminar</th>
        </tr>
      </thead>
   
  <?php $indice=1 ;
      foreach ($usuariopropietario->result() as $row) {
      ?>
      <tr>
        <td><?php echo $indice;?></td>
        <td><?php echo $row->priApellido;?></td>
        <td><?php echo $row->nombres;?></td>
        <td><?php echo $row->email;?></td>
        <td><?php echo $row->ci;?></td>
        <td><?php echo $row->login;?></td>
        <td><?php echo $row->password;?></td>
        <td><?php echo $row->estado;?></td>
        <td>
          <?php echo form_open_multipart('welcome/modificar');?>
            <input type="hidden" name="idPropietario" value="<?php echo $row->idPropietario;?>">
            <button type="submit" class="btn btn-primary">Modificar</button>
          <?php echo form_close(); ?>
        </td>
        <td>
          <?php echo form_open_multipart('welcome/eliminardb');?>
            <input type="hidden" name="idPropietario" value="<?php echo $row->idPropietario;?>">
            <input type="hidden" name="nombres" value="<?php echo $row->nombres;?>">
            <button type="submit" class="btn btn-primary">Eliminar</button>
          <?php echo form_close(); ?>
        </td>

      <?php
      $indice++;
      
      }
      ?>
    </table>
  </div>
 



  <footer class="footer footer-black  footer-white ">
    <div class="container">
      <div class="row">
        <div class="credits ml-auto">
          <span class="copyright">
            <script>
                  
            </script> COCHABAMBA <i class="fa fa-heart heart"></i> BOLIVIA
          </span>
        </div>
      </div>
    </div>
  </footer>


  <!--   Core JS Files   -->
  <script src="<?=base_url()?>/bootstrap/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/bootstrap/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/bootstrap/js/core/bootstrap.min.js" type="text/javascript"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="<?=base_url()?>/bootstrap/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="<?=base_url()?>/bootstrap/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    <script src="<?=base_url()?>/bootstrap/js/plugins/moment.min.js"></script>
    <script src="<?=base_url()?>/bootstrap/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Control Center for Paper Kit: parallax effects, scripts for the example pages etc -->
    <script src="<?=base_url()?>/bootstrap/js/paper-kit.js?v=2.2.0" type="text/javascript"></script>
    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
</body>

</html>