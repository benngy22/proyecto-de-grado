<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>/bootstrap/img/icono.png">
  <link rel="icon" type="image/png" href="<?=base_url()?>/bootstrap/img/icono.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Iniciar Seccion
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="<?=base_url()?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?=base_url()?>/bootstrap/css/paper-kit.css?v=2.2.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?=base_url()?>/bootstrap/demo/demo.css" rel="stylesheet" />
</head>

<body class="register-page sidebar-collapse" >
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg fixed-top navbar-transparent " color-on-scroll="300">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand"  rel="tooltip" title="Coded by Creative Tim" data-placement="bottom" target="_blank">
          COCHAturismo
        </a>
      </div>
    </div>
  </nav>
  
  <div class="page-header section-dark" style="background-image: url('<?=base_url()?>/bootstrap/img/pantalla.jpg')">
  
    <div class="filter"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-4 ml-auto mr-auto">
          <div class="card card-register">
            <h3 class="title mx-auto">INICIAR SECCION</h3>
            <form action="<?=base_url()?>index.php/Welcome/validarUsuario" method="post" class="register-form">
            
              <label>LOGIN</label>
              <div class="input-group form-group-no-border">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="nc-icon nc-email-85"></i>
                  </span>
                </div>
                <input type="text" class="form-control" placeholder="Ingrese Login" name="login">
              </div>
              <label>PASSWORD</label>
              <div class="input-group form-group-no-border">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="nc-icon nc-key-25"></i>
                  </span>
                </div>
                <input type="password" class="form-control" placeholder="Ingrese Password" name="password">
              </div>
              <button type="submit" class="btn btn-danger btn-block btn-round">Register</button>
            </form>
            <div class="forgot">
              <a href="#" class="btn btn-link btn-danger">Olvidaste tu contraseña?</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer register-footer text-center">
      <h6>©
        <script>
          document.write(new Date().getFullYear())
        </script>, Cochabamba <i class="fa fa-heart heart"></i> bolivia</h6>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?=base_url()?>/bootstrap/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="<?=base_url()?>/bootstrap/js/core/popper.min.js" type="text/javascript"></script>
  <script src="<?=base_url()?>/bootstrap/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="<?=base_url()?>/bootstrap/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?=base_url()?>/bootstrap/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="<?=base_url()?>/bootstrap/js/plugins/moment.min.js"></script>
  <script src="<?=base_url()?>/bootstrap/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!-- Control Center for Paper Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?=base_url()?>/bootstrap/js/paper-kit.js?v=2.2.0" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
</body>

</html>