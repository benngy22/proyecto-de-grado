<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>/bootstrap/img/icono.png">
  <link rel="icon" type="image/png" href="<?=base_url()?>/bootstrap/img/icono.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    COCHAturismo
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="<?=base_url()?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?=base_url()?>/bootstrap/css/paper-kit.css?v=2.2.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?=base_url()?>/bootstrap/demo/demo.css" rel="stylesheet" />
</head>

<body class="index-page sidebar-collapse">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg fixed-top navbar-transparent " color-on-scroll="300">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="https://demos.creative-tim.com/paper-kit/index.html" rel="tooltip" title="Coded by Creative Tim" data-placement="bottom" target="_blank">
          COCHAturismo
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <ul class="navbar-nav">
          
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
              <i class="fa fa-facebook-square"></i>
              <p class="d-lg-none">Facebook</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
              <i class="fa fa-instagram"></i>
              <p class="d-lg-none">Instagram</p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href=""  class="nav-link"><i class="nc-icon nc-tap-01"></i> Inicio</a>
          </li>
          <li class="nav-item">
            <a href="#images"  class="nav-link"><i class="nc-icon nc-send"></i> Lugares Turisticos</a>
          </li>
          <li class="nav-item">
            <a href="#fotografias"  class="nav-link"><i class="nc-icon nc-image"></i> Fotografias</a>
          </li>
          <li class="nav-item">
            <a href=""  class="nav-link"><i class="nc-icon nc-bus-front-12"></i> Paradas</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>index.php/Welcome/listausuarios"  class="btn btn-danger btn-round"><i class="nc-icon nc-single-02"></i> Lista Usuarios</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>index.php/Welcome/logout"  class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i> Log out</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  
  <!-- End Navbar -->
  <div class="page-header section-dark" style="background-image: url('<?=base_url()?>/bootstrap/img/fondo.jpg')">
    <div class="filter"></div>
    <div class="content-center">
      <div class="container">
        <div class="title-brand">
          <h1 class="presentation-title">COCHAturismo</h1>
          <div class="fog-low">
            <img src="<?=base_url()?>/bootstrap/img/fog-low.png" alt="">
          </div>
          <div class="fog-low right">
            <img src="<?=base_url()?>/bootstrap/img/fog-low.png" alt="">
          </div>
        </div>
        <h2 class="presentation-subtitle text-center">CIUDAD GASTRONOMICA DE BOLIVIA ! </h2>
        <h2 class="presentation-subtitle text-center">Cochabamba ciudad de todos ! </h2>
      </div>
    </div>
    <div class="footer register-footer text-center">
      <h6>©
        <script>
          document.write(new Date().getFullYear())
        </script>, Cochabamba <i class="fa fa-heart heart"></i> bolivia</h6>
    </div>
    
  </div>
  
  <div class="container tim-container">
      <br/>
      <div id="images">
        <div class="container">
          <div class="title">
            <h3>Lugares Turisticos</h3>
          </div>
          <div class="row">
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/cristo.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3>Cristo de la Concordia </h3>
                <a href="l" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/catedral.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3>Catedral </h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/museo1.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3>Museo UMSS </h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/aguas.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3> Aguas Danzantes</h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/teleferico.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3> Teleferico</h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/arco.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3>Arco Calvario </h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
          </div>
          <div class="title">
            <h3>Plazas</h3>
          </div>
          <div class="row">
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/banderas.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3>Las Banderas </h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/calacala.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3>Cala Cala </h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/sucre.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3>Sucre </h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/colon.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3> Colon</h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/corazon.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3> Corazonistas</h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              
              <img src="<?=base_url()?>bootstrap/img/plaza14.jpg" class="img-rounded img-responsive" alt="Rounded Image">
              <div class="img-details">
                <h3>Principal </h3>
                <a href="#" target="_blank" class="btn btn-danger btn-round"><i class="nc-icon nc-map-big"></i> Mas Información</a>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <div class="section javascript-components">
      <div class="container">
        <div class="row" id="modals">
          <br>
          <div class="col-md-6">
            <div class="title">
              <h3>Fechas</h3>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <div class="input-group date" id="datetimepicker">
                    <input type="text" class="form-control datetimepicker" placeholder="27/03/2019" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <span class="glyphicon glyphicon-calendar"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="title">
          <h3>Provincias</h3>
        </div>
      </div>
    </div>
    <div class="section pt-o" id="fotografias">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <div class="card page-carousel">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="9"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="10"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/cercado.jpg" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>CERCADO</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/arani.jpg" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>ARANI</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/capinota.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>CAPINOTA</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/carrasco.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>CARRASCO</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/chapare.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>CHAPARE</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/cliza.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>CLIZA</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/mizque.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>MIZQUE</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/quillacollo.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>QUILLACOLLO</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/tarata.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>TARATA</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/tiraque.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>TIRAQUE</b></p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="<?=base_url()?>bootstrap/img/punata.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <p><b>PUNATA</b></p>
                    </div>
                  </div>
                </div>
                <a class="left carousel-control carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <footer class="footer footer-black  footer-white ">
      <div class="container">
        <div class="row">
          <nav class="footer-nav">
            <ul>
              <li>
                <a href="#" target="_blank">Aniversarios</a>
              </li>
              <li>
                <a href="#" target="_blank">Ferias</a>
              </li>
            </ul>
          </nav>



          <div class="credits ml-auto">
            <span class="copyright">
              ©
                <script>
                  document.write(new Date().getFullYear())
                </script><b>, COCHABAMBA <i class="fa fa-heart heart"></i> BOLIVIA</b>
            </span>
          </div>
        </div>
      </div>
    </footer>
    <!--   Core JS Files   -->
    <script src="<?=base_url()?>/bootstrap/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/bootstrap/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/bootstrap/js/core/bootstrap.min.js" type="text/javascript"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="<?=base_url()?>/bootstrap/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="<?=base_url()?>/bootstrap/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    <script src="<?=base_url()?>/bootstrap/js/plugins/moment.min.js"></script>
    <script src="<?=base_url()?>/bootstrap/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Control Center for Paper Kit: parallax effects, scripts for the example pages etc -->
    <script src="<?=base_url()?>/bootstrap/js/paper-kit.js?v=2.2.0" type="text/javascript"></script>
    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <script>
      $(document).ready(function() {

        if ($("#datetimepicker").length != 0) {
          $('#datetimepicker').datetimepicker({
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            }
          });
        }

        function scrollToDownload() {

          if ($('.section-download').length != 0) {
            $("html, body").animate({
              scrollTop: $('.section-download').offset().top
            }, 1000);
          }
        }
      });
    </script>
</body>

</html>