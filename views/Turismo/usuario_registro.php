
<div class="page-header section-dark" style="background-image: url('./bootstrap/img/pantalla.jpg')">
<div class="container-fluid"> 
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
    <form action="<?= base_url()?>index.php/welcome/insertardb" method="POST">
    <div class="form-group">
      <label for="nombre">Primer Apellido</label>
      <input type="text" class="form-control" name="priApellido" placeholder="Apellido Paterno">
    </div>
    <div class="form-group">
      <label for="nota">Segundo Apellido</label>
      <input type="text" class="form-control" name="segApellido" placeholder="Apellido Materno">
    </div>
    <div class="form-group">
      <label for="nota">Nombres</label>
      <input type="text" class="form-control" name="nombres" placeholder="Ingrese Nombres">
    </div>
    <div class="form-group">
      <label for="nota">Correo Email</label>
      <input type="email" class="form-control" name="email" placeholder="Ingrese Correo Electronico">
    </div>
    <div class="form-group">
      <label for="nota">C.I.</label>
      <input type="text" class="form-control" name="ci" placeholder="Numero de Carnet">
    </div>
    <div class="form-group">
      <label for="nota">Login</label>
      <input type="text" class="form-control" name="login" placeholder="Ingrese Nombre de Usuario">
    </div>
    <div class="form-group">
      <label for="nota">Password</label>
      <input type="password" class="form-control" name="password" placeholder="Ingrese Contraseña">
    </div>
    <div class="form-group">
      <label for="nota">Telefono</label>
      <input type="text" class="form-control" name="telefono" placeholder="Ingrese Telefono">
    </div>
    
    <button type="submit" class="btn btn-primary">Registrar</button>
  </form> 
    </div>
  </div>
</div>
</div>