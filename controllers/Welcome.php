<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('login')) {
            redirect('Welcome/panel','refresh');
        }
        else {
			$this->load->view('head');
            $this->load->view('Turismo/login');
        }
	}

	public function validarUsuario()
    {
        $login=$_POST['login'];
        $password=md5($_POST['password']);
        $consulta=$this->usuario_model->validar($login,$password);

        if ($consulta->num_rows()>0) {
            foreach ($consulta->result() as $row) {
				$this->session->set_userdata('login',$row->login);
			}
			redirect('Welcome/panel','refresh');
        }
        else {
            redirect('Welcome/index','refresh');
        }
	}
	
	public function panel()
    {
        if ($this->session->userdata('login')) {
			
			$this->load->view('Turismo/index');
			$this->load->view('footer');
			
        }
        else {
            redirect('Welcome/index','refresh');
        }
	}

	public function listausuarios()
	{
		if ($this->session->userdata('login')) {
			$data['usuariopropietario']=$this->usuario_model->retornarUsuarios();
            $this->load->view('Turismo/listausuarios',$data);
        }
        else {
            $this->load->view('Turismo/login');
        }
	}
	
	public function modificar()
	{
		$idPropietario=$_POST['idPropietario'];
		$data['usuariopropietario']=$this->usuario_model->recuperarUsuario($idPropietario);
		$this->load->view('head');
		$this->load->view('Turismo/usuario_modificar',$data);
		$this->load->view('footer');
	}
	public function insertar()
	{
		$this->load->view('head');
		$this->load->view('Turismo/usuario_insertar');
		$this->load->view('footer');

	}
	public function modificardb()
	{
		
		
		$idPropietario=$_POST['idPropietario'];

		$priApellido=$_POST['priApellido'];
		$segApellido=$_POST['segApellido'];
		$nombres=$_POST['nombres'];
		$email=$_POST['email'];
		$ci=$_POST['ci'];
		$login=$_POST['login'];
		$password=md5($_POST['password']);
		$telefono=$_POST['telefono'];
		$data['priApellido']=$priApellido;
		$data['segApellido']=$segApellido;
		$data['nombres']=$nombres;
		$data['email']=$email;
		$data['ci']=$ci;
		$data['login']=$login;
		$data['password']=$password;
		$data['telefono']=$telefono;
		$this->usuario_model->modificarUsuario($idPropietario,$data);
		$this->load->view('head');
		redirect('Welcome/listausuarios','refresh');
		$this->load->view('footer');

	}
  public function registro()
	{
		$this->load->view('head');
		$this->load->view('Turismo/usuario_registro');
		$this->load->view('footer');
	}
	
	public function insertardb()
	{
		$priApellido=$_POST['priApellido'];
		$segApellido=$_POST['segApellido'];
		$nombres=$_POST['nombres'];
		$email=$_POST['email'];
		$ci=$_POST['ci'];
		$login=$_POST['login'];
		$password=md5($_POST['password']);
		$telefono=$_POST['telefono'];
		$data['priApellido']=$priApellido;
		$data['segApellido']=$segApellido;
		$data['nombres']=$nombres;
		$data['email']=$email;
		$data['ci']=$ci;
		$data['login']=$login;
		$data['password']=$password;
		$data['telefono']=$telefono;
		$this->usuario_model->insertarUsuario($data);
		$this->load->view('head');
		$this->load->view('Turismo/usuario_insertado',$data);
		$this->load->view('footer');

	}
	public function eliminardb()
	{
		$idPropietario=$_POST['idPropietario'];
		$nombres=$_POST['nombres'];
		$data['usuariopropietario']=$nombres;
		$this->usuario_model->eliminarUsuario($idPropietario);
		$this->load->view('head');
		$this->load->view('Turismo/usuario_eliminado',$data);
		$this->load->view('footer');
	}

	public function logout()
    {
        $this->session->sess_destroy();
        redirect('Welcome/index','refresh');
	}
	
	
  
}
