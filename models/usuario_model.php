<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model {
//no existe el metodo index
//active record permite hacer consultas a la bd	
	public function retornarUsuarios()
	{
        $this->db->SELECT('idPropietario,priApellido,segApellido,nombres,email,ci,login,password,telefono,estado');
        $this->db->FROM('usuariopropietario');
        $this->db->ORDER_BY('nombres','desc');
        return $this->db->get();
    }
    public function insertarUsuario($data)
    {
        $this->db->INSERT('usuariopropietario',$data);
    }
    public function recuperarUsuario($idPropietario)
    {
        $this->db->SELECT('idPropietario,priApellido,segApellido,nombres,email,ci,login,password,telefono');
        $this->db->FROM('usuariopropietario');
        $this->db->WHERE('idPropietario',$idPropietario);
        return $this->db->get();
    }
    public function modificarUsuario($idPropietario,$data)
    {
        $this->db->WHERE('idPropietario',$idPropietario);
        $this->db->UPDATE('usuariopropietario',$data);
    }
    public function eliminarUsuario($idPropietario)
    {
        $this->db->WHERE('idPropietario',$idPropietario);
        $this->db->DELETE('usuariopropietario');
    }

    public function validar($login,$password)
    {
        $this->db->SELECT('*');
        $this->db->FROM('usuariopropietario');
        $this->db->WHERE('login',$login);
        $this->db->WHERE('password',$password);
        return $this->db->get();
    }
}
